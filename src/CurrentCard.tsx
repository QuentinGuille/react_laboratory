import { useEffect, useState } from "react";
import { Action, Card, Resource, Stage } from "./type";

interface myProps {
  stage: Stage;
  resourceToText: (resource: Resource[] | null, emoji?: string) => JSX.Element;
  cardOrigin: Card;
  action: Action;
  updateCurrentCardActionResource: (index: number) => void;
}

function CurrentCardComponent(props: myProps) {
  const [selectedResource, setSelectedResource] = useState(-1);

  useEffect(() => {
    props.updateCurrentCardActionResource(selectedResource);
  }, [selectedResource, props]);

  const selectResource = (resources: null | Resource[]): JSX.Element => {
    let resourcesElement = <></>;

    if (resources === null || props.action === "unknown") {
      return resourcesElement;
    }

    resourcesElement = (
      <>
        {resources.map((resource, index) => {
          if (index > 0) {
            return (
              <span
                key={index}
                onClick={() =>
                  selectedResource === index
                    ? setSelectedResource(-1)
                    : setSelectedResource(index)
                }
              >
                {" "}
                / {Array(resource.cerveau).fill("🧠")}
                {Array(resource.potion).fill("🧪")}
                {Array(resource.rouage).fill("🔩")}
              </span>
            );
          } else {
            return (
              <span
                key={index}
                onClick={() =>
                  selectedResource === index
                    ? setSelectedResource(-1)
                    : setSelectedResource(index)
                }
                style={{
                  cursor: "pointer",
                }}
              >
                {resource.cerveau === resource.potion &&
                resource.potion === resource.rouage &&
                resource.rouage === 0 &&
                resource.danger === 0
                  ? "gratuit"
                  : ""}
                {Array(resource.cerveau).fill("🧠")}
                {Array(resource.potion).fill("🧪")}
                {Array(resource.rouage).fill("🔩")}
                {resource.danger !== 0 && `${resource.danger}:⏱️`}
              </span>
            );
          }
        })}
      </>
    );

    return resourcesElement;
  };

  const currentAction = props.action;

  if (currentAction === "unknown") {
    return <></>;
  }

  const stageAction = props.stage[currentAction];

  return (
    <div>
      <div className="stage item0" key={props.stage.id}>
        <div className="textOverlay">{props.stage.id + 1}</div>
        <div className="resource">{selectResource(stageAction)}</div>
        <div className="name">{props.stage.name}</div>
        <div className="resource">
          {selectedResource === -1
            ? null
            : props.resourceToText(
                stageAction === null ? null : [stageAction[selectedResource]]
              )}
        </div>
      </div>
    </div>
  );
}

export default CurrentCardComponent;
