import React from "react";
import { Card, Resource, Stage, Action } from "./type";

interface myProps {
  stage: Stage;
  resourceToText: (resource: Resource[] | null, emoji?: string) => JSX.Element;
  resourceToTextWithNumber: (
    resource: Resource[] | null,
    emoji?: string
  ) => JSX.Element;
  cardOrigin: Card;
  disabled: boolean;
  openModal: (open: boolean) => void;
  setCurrentCardAction: (card: Card, action: Action) => void;
  hidden: boolean;
  order: number;
}

function StageComponent(props: myProps) {
  return (
    <div className={"stage" + ` item${props.order}`} key={props.stage.id}>
      <div className="textOverlay">{props.stage.id + 1}</div>

      <div className={(props.hidden ? "blurred " : "") + "fullsize"}>
        <div className="resource">
          {props.resourceToText(props.stage.resources)}
        </div>
        <div className="name" style={{ position: "relative" }}>
          {props.stage.name}
          <div className="textOverlay3">
            {Array(props.stage.bonus.engin)
              .fill("⚡")
              .map((v, i) => (
                <React.Fragment key={i}>
                  {v}
                  <br />
                </React.Fragment>
              ))}
            {Array(props.stage.bonus.potion)
              .fill("🧬")
              .map((v, i) => (
                <React.Fragment key={i}>
                  {v}
                  <br />
                </React.Fragment>
              ))}
            {props.stage.hidden && (
              <>
                <span>👁️</span>
                <br />
              </>
            )}
          </div>
        </div>
        <div className={props.disabled ? "action disabled" : "action"}>
          {props.stage.stock === null || (
            <div className="stock first-row">
              <button
                disabled={props.disabled}
                onClick={() => {
                  props.setCurrentCardAction(props.cardOrigin, "stock");
                }}
              >
                {props.resourceToTextWithNumber(props.stage.stock, "↘️")}
              </button>
            </div>
          )}

          {props.stage.return === null || (
            <div
              className={
                (props.stage.stock === null ? "first-row" : "second-row") +
                " " +
                (props.stage.stock === null
                  ? "stock"
                  : props.stage.pivot === null
                  ? "stock"
                  : "no-stock")
              }
            >
              <button
                disabled={props.disabled}
                onClick={() => {
                  props.setCurrentCardAction(props.cardOrigin, "return");
                }}
              >
                {props.resourceToTextWithNumber(props.stage.return, "↔️")}
              </button>
            </div>
          )}

          {props.stage.pivot === null || (
            <div
              className={
                (props.stage.stock === null
                  ? props.stage.return === null
                    ? "first-row"
                    : "second-row"
                  : "second-row") +
                " " +
                (props.stage.stock === null
                  ? "stock"
                  : props.stage.return === null
                  ? "stock"
                  : "no-stock")
              }
            >
              <button
                disabled={props.disabled}
                onClick={() => {
                  props.setCurrentCardAction(props.cardOrigin, "pivot");
                }}
              >
                {props.resourceToTextWithNumber(props.stage.pivot, "↕️")}
              </button>
              <br />
            </div>
          )}

          {props.stage.victory === null || (
            <div className="stock first-row">
              <button
                disabled={props.disabled}
                onClick={() => {
                  props.setCurrentCardAction(props.cardOrigin, "victory");
                }}
              >
                {props.resourceToTextWithNumber(
                  props.stage.victory,
                  "Victoire"
                )}
              </button>
            </div>
          )}
        </div>
        {props.stage.score !== 0 && <br />}
        {props.stage.score !== 0 && (
          <div className="textOverlayBR">{props.stage.score}⭐</div>
        )}
        {props.stage.special !== null && (
          <span>{`* : ${props.stage.special.multiplier}⭐ pour chaque ${
            props.stage.special.resources === "engin" ? "⚡" : "🧬"
          }`}</span>
        )}
      </div>
    </div>
  );
}

export default StageComponent;
