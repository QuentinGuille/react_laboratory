import Game from "./Game";
import "./App.css";
import { useState } from "react";

import NavComponent from "./NavComponent";

export const App = () => {
  const [dangerLevel, setDangerLevel] = useState(0);

  const updateDangerLevelBy = (change: number) =>
    setDangerLevel(dangerLevel + change);

  return (
    <>
      <NavComponent dangerLevel={dangerLevel} />
      <main>
        <Game
          dangerLevel={dangerLevel}
          updateDangerLevelBy={(change: number) => updateDangerLevelBy(change)}
        />
      </main>
    </>
  );
};

export default App;
