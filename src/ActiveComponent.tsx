import { useState } from "react";
import { Card, Resource, Stage } from "./type";

interface myProps {
  stage: Stage;
  resourceToText: (resource: Resource[] | null, emoji?: string) => JSX.Element;
  cardOrigin: Card;
  addActiveStockedCard: (card: Card, index: number) => void;
  removeActiveStockedCard: (card: Card) => void;
  removeCardFromStock: (card: Card) => void;
  isDiscardNeeded: boolean;
  item: number;
}

function ActiveComponent(props: myProps) {
  const [selectedResource, setSelectedResource] = useState(-1);

  const selectResource = (resources: null | Resource[]): JSX.Element => {
    let resourcesElement = <></>;

    if (resources === null) {
      return resourcesElement;
    }

    const onClick = (index: number) => {
      if (selectedResource === index) {
        setSelectedResource(-1);
        console.log("remove");
        props.removeActiveStockedCard(props.cardOrigin);
      } else {
        setSelectedResource(index);
        console.log("add");
        props.addActiveStockedCard(props.cardOrigin, index);
      }
    };

    resourcesElement = (
      <>
        {resources.map((resource, index) => {
          if (index > 0) {
            return (
              <span key={index} onClick={() => onClick(index)}>
                {" "}
                / {Array(resource.cerveau).fill("🧠")}
                {Array(resource.potion).fill("🧪")}
                {Array(resource.rouage).fill("🔩")}
              </span>
            );
          } else {
            return (
              <span
                key={index}
                onClick={() => onClick(index)}
                style={{
                  cursor: "pointer",
                }}
              >
                {resource.cerveau === resource.potion &&
                resource.potion === resource.rouage &&
                resource.rouage === 0 &&
                resource.danger === 0
                  ? "gratuit"
                  : ""}
                {Array(resource.cerveau).fill("🧠")}
                {Array(resource.potion).fill("🧪")}
                {Array(resource.rouage).fill("🔩")}
                {resource.danger !== 0 && `${resource.danger}:⏱️`}
              </span>
            );
          }
        })}
      </>
    );

    return resourcesElement;
  };

  return (
    <div className={`stage item${props.item}`} key={props.stage.id}>
      <div className="textOverlay">{props.stage.id + 1}</div>
      <div className="resource">{selectResource(props.stage.resources)}</div>
      <div className="name">{props.stage.name}</div>

      {selectedResource === -1 ? (
        <button
          disabled={!props.isDiscardNeeded || !props.cardOrigin.skipStock}
          onClick={() => {
            props.removeCardFromStock(props.cardOrigin);
          }}
        >
          remove
        </button>
      ) : (
        <div className="resource">
          {props.resourceToText(
            props.stage.resources === null
              ? null
              : [props.stage.resources[selectedResource]]
          )}
        </div>
      )}
    </div>
  );
}

export default ActiveComponent;
