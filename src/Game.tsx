import { useCallback, useEffect, useState } from "react";

import baseCard from "./assets/cards/base.json";
//import testCard from "./assets/cards/test_card.json";
//import assistantCard from "./assets/cards/assistant.json";
import assistantCard from "./assets/cards/assistant_test.json";
import productionCard from "./assets/cards/production.json";
import dangerCard from "./assets/cards/danger.json";
import StageComponent from "./StageComponent";
import ActiveComponent from "./ActiveComponent";
import CurrentCardComponent from "./CurrentCard";
import { Card, Action, Resource } from "./type";
import DifficultyComponent from "./DifficultyComponent";

interface GameProps {
  dangerLevel: number;
  updateDangerLevelBy: (change: number) => void;
}

function Game(props: GameProps) {
  // card loaded
  const [isLoaded, setIsLoaded] = useState(false);

  const [difficulty, setDifficulty] = useState(0);

  const updateDifficulty = (change: number) => {
    console.log(change);
    setDifficulty(change);
  };

  // isShuffled si les cartes sont melangé true sinon false
  const [isShuffled, setIsShuffled] = useState(false);

  // shuffled si les cartes doivent être mélangé, true si elle doivent l'être, false sinon
  const [shuffled, setShuffled] = useState(false);

  // toutes les cartes du jeu
  const [cards, setCards] = useState(Array<Card>);

  // ouverture de la modal de selection de carte
  const [isOpen, setIsOpen] = useState(false);
  const [isDiscardNeeded, setIsDiscardNeeded] = useState(false);

  // carte qui sont actuellement stocker
  const [currentStockedCard, setCurrentStockedCard] = useState(
    [] as Array<Card>
  );
  const [stockCardToAdd, setStockCardToAdd] = useState([] as Array<Card>);
  const [stockCardToRemove, setStockCardToRemove] = useState([] as Array<Card>);

  // carte qui sont selectionné parmis les stocker
  const [currentActiveStockedCard, setActiveCurrentStockedCard] = useState(
    [] as Array<{ card: Card; resourceIndex: number }>
  );

  const [winCondition, setWinCondition] = useState(false);

  // carte qui va être jouer avec l'action qui va
  const [currentCardAction, setCurrentCardAction] = useState({
    card: {} as Card,
    action: "unknown" as Action,
  });

  const [currentCardActionResource, setCurrentCardActionResource] =
    useState(-1);

  const cardIsUsed = useCallback(
    (card: Card, paid: boolean, wasUpgrade: boolean) => {
      if (card.id === "D" && paid) {
        return;
      }

      console.log(
        `card_${card.id}_${card.stage[card.currentStage].id}_${
          card.stage[card.currentStage].name
        } was used`
      );

      if (wasUpgrade && card.id[0] === "M") {
        setStockCardToAdd([card]);
      }

      const idToRemove = card.id;
      const tmp = [...cards].filter((card) => card.id !== idToRemove);
      tmp.push(card);

      setCurrentCardAction(() => {
        return { card: tmp[0], action: "unknown" };
      });

      if (!card.skip && !paid && (card.id === "P" || card.id === "R")) {
        const indexDanger = cards.findIndex((v) => v.id === "D");
        updateCardStage(
          cards[indexDanger],
          cards[indexDanger].currentStage === 1 ? "return" : "pivot",
          false,
          false
        );
        props.updateDangerLevelBy(1);
      }

      setCards(tmp);
    },
    [cards, props]
  );

  const addStockedCard = useCallback(
    (card: Card, paid: boolean) => {
      if (currentStockedCard.length >= 4) {
        setIsDiscardNeeded(true);
        setIsOpen(true);
        return;
      }
      console.log(
        `card_${card.id}_${
          card.stage[card.currentStage].name
        } added to stocked card`
      );
      setStockCardToAdd([card]);
      cardIsUsed(card, paid, false);
      return;
    },
    [cardIsUsed, currentStockedCard]
  );

  const updateCardStage = useCallback(
    (card: Card, action: Action, paid: boolean, cardUsed: boolean = true) => {
      if (action === "unknown") {
        return;
      }

      if (action === "victory") {
        setWinCondition(true);
        return;
      }

      const previousStage = card.currentStage;

      if (action === "pivot" && previousStage % 2 === 1) {
        card.currentStage = card.currentStage - 1;
      }
      if (action === "pivot" && previousStage % 2 === 0) {
        card.currentStage = card.currentStage + 1;
      }
      if (action === "return") {
        card.currentStage = (card.currentStage + 2) % 4;
      }

      console.log(
        `card_${card.id}_${
          card.stage[card.currentStage].name
        } updated from stage ${previousStage} to stage ${card.currentStage} `
      );

      if (card.id === "D") {
        if (
          (previousStage === 2 && card.currentStage === 3) ||
          (previousStage === 3 && card.currentStage === 1) ||
          (previousStage === 1 && card.currentStage === 0)
        ) {
          props.updateDangerLevelBy(-1);
        }
      }

      if (cardUsed) {
        cardIsUsed(card, paid, true);
      }
    },
    [cardIsUsed, props]
  );

  const load = () => {
    // console.log("load");
    setIsLoaded(false);
    props.updateDangerLevelBy(-props.dangerLevel);
    setWinCondition(false);
    setActiveCurrentStockedCard([]);
    setCurrentStockedCard([]);
    setIsOpen(false);
    setCards([] as Card[]);
    setCurrentCardAction({ card: {} as Card, action: "unknown" });
  };

  const shuffle = (cards: Array<Card>) => {
    let currentIndex = cards.length;

    while (currentIndex !== 0) {
      const randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      [cards[currentIndex], cards[randomIndex]] = [
        cards[randomIndex],
        cards[currentIndex],
      ];
    }
  };

  useEffect(() => {
    if (cards.length === 0 || (shuffled && !isShuffled)) {
      const loadBase = () => JSON.parse(JSON.stringify(baseCard));
      const loadAssist = () => JSON.parse(JSON.stringify(assistantCard));
      const loadProduction = () => JSON.parse(JSON.stringify(productionCard));
      const loadDanger = () => JSON.parse(JSON.stringify(dangerCard));

      const base = loadBase() as Card[];
      if (shuffled) {
        shuffle(base);
        setIsShuffled(true);
        setShuffled(false);
      }
      const assist = loadAssist() as Card[];
      const production = loadProduction() as Card;
      const danger = loadDanger() as Card;

      const newAssist =
        difficulty === 3
          ? []
          : difficulty === 2
          ? [assist[1]]
          : difficulty === 1
          ? [assist[1], assist[2]]
          : [...assist];

      const array = [...base, ...newAssist, production, danger];

      setCards(array);
      setCurrentCardAction({ card: array[0] as Card, action: "unknown" });
      setIsLoaded(true);
    }
  }, [cards, shuffled, isShuffled]);
  useEffect(() => {
    const loadBase = () => JSON.parse(JSON.stringify(baseCard));
    const loadAssist = () => JSON.parse(JSON.stringify(assistantCard));
    const loadProduction = () => JSON.parse(JSON.stringify(productionCard));
    const loadDanger = () => JSON.parse(JSON.stringify(dangerCard));

    const base = loadBase() as Card[];
    if (isShuffled) {
      shuffle(base);
    }
    const assist = loadAssist() as Card[];
    const production = loadProduction() as Card;
    const danger = loadDanger() as Card;

    const newAssist =
      difficulty === 3
        ? []
        : difficulty === 2
        ? [assist[1]]
        : difficulty === 1
        ? [assist[1], assist[2]]
        : [...assist];

    const array = [...base, ...newAssist, production, danger];

    setCards(array);
    setCurrentCardAction({ card: array[0] as Card, action: "unknown" });
    setIsLoaded(true);
  }, [difficulty]);

  useEffect(() => {
    if (!isLoaded) {
      return;
    }

    const card = currentCardAction.card;
    const action = currentCardAction.action;

    if (action === "unknown") {
      return;
    }
    const resources = card.stage[card.currentStage][action];

    if (resources === null) {
      return;
    }

    // if card is free we can directly perform action

    if (
      resources.length === 1 &&
      resources[0].cerveau === 0 &&
      resources[0].potion === 0 &&
      resources[0].rouage === 0 &&
      resources[0].danger === 0
    ) {
      if (action === "stock") {
        addStockedCard(card, true);
        return;
      }
      if (action === "pivot" || action === "return") {
        updateCardStage(card, action, true);
        return;
      }
    }

    // if card is not free open modal

    setIsOpen(true);
    return;
  }, [currentCardAction, isLoaded, addStockedCard, updateCardStage]);

  useEffect(() => {
    if (isOpen && currentCardAction.action === "unknown") {
      setIsOpen(false);
      return;
    }
  }, [isOpen, currentCardAction.action]);

  const addActiveStockedCard = (card: Card, resourceIndex: number) => {
    console.log(
      `card_${card.id}_${
        card.stage[card.currentStage].name
      } added to ** ACTIVE ** stocked card`
    );

    if (currentActiveStockedCard.length >= 4) {
      return;
    }
    const tmp = [...currentActiveStockedCard];
    tmp.push({ card, resourceIndex });
    setActiveCurrentStockedCard(tmp);
    return;
  };

  const removeActiveStockedCard = (card: Card) => {
    console.log(
      `card_${card.id}_${
        card.stage[card.currentStage].name
      } removed from ** ACTIVE ** stocked card`
    );
    const tmp = [...currentActiveStockedCard].filter(
      (tmpCard) => tmpCard.card.id !== card.id
    );
    setActiveCurrentStockedCard(tmp);
    return;
  };

  const removeMultipleStockedCard = (card: Card[]) => {
    setStockCardToRemove(card);
    return;
  };

  const removeAllActiveStockedCard = () => {
    console.log(
      "all active stocked card are removed",
      currentActiveStockedCard.map(
        (v) => `${v.card.id} ${v.card.stage[v.card.currentStage].name}`
      )
    );
    removeMultipleStockedCard(currentActiveStockedCard.map((v) => v.card));
    setActiveCurrentStockedCard([]);
  };

  useEffect(() => {
    if (currentStockedCard.length > 0) {
      currentStockedCard.forEach((card) => {
        if (cards[0].id === card.id) {
          setStockCardToRemove([card]);
          cardIsUsed(card, true, false);
        }
      });
    }
  }, [cards, currentStockedCard, cardIsUsed]);

  useEffect(() => {
    if (currentStockedCard.length === 0 && stockCardToAdd.length === 0) {
      // si c'est vide et pas de carte à ajouter on fait rien
      return;
    }
    if (
      currentStockedCard.length > 0 &&
      stockCardToAdd.length === 0 &&
      stockCardToRemove.length === 0
    ) {
      // si c'est pas vide et pas de carte à ajouter ou pas de carte à retirer on fait rien
      return;
    }
    const idsToRemove = stockCardToRemove.map((value) => value.id);
    const tmp = [...currentStockedCard].filter(
      (card) => !idsToRemove.includes(card.id)
    );
    tmp.push(...stockCardToAdd);

    setStockCardToAdd([]);
    setStockCardToRemove([]);
    setCurrentStockedCard(tmp);
  }, [stockCardToRemove, stockCardToAdd, currentStockedCard]);

  return (
    <article>
      {props.dangerLevel === 0 && (
        <>
          <div className="title">
            <div className="right">
              <p>RE</p>
              <p>L</p>
            </div>
            <div className="center">
              <p>A</p>
            </div>
            <div className="left">
              <p>CT</p>
              <p>BORATORY</p>
            </div>
          </div>
          <DifficultyComponent
            difficulty={difficulty}
            updateDifficulty={(change: number) => updateDifficulty(change)}
          />
        </>
      )}

      {winCondition && (
        <div>
          <h2>Congratulation</h2>
          <button onClick={() => load()}>Exit</button>
        </div>
      )}

      {props.dangerLevel === 0 && (
        <div className="newGame">
          <button onClick={() => props.updateDangerLevelBy(1)}>
            <h3>First Game ?</h3>
          </button>
          <button
            onClick={() => {
              setShuffled(true);
              props.updateDangerLevelBy(1);
            }}
          >
            <h3>New Game</h3>
          </button>
        </div>
      )}
      {props.dangerLevel >= 5 && (
        <div>
          <h2>You loosed</h2>
          <button onClick={() => load()}>Exit</button>
        </div>
      )}

      {props.dangerLevel > 0 && props.dangerLevel < 5 && !winCondition && (
        <>
          {isOpen && (
            <div id="myModal" className="modal" style={{ display: "block" }}>
              <div className="modal-content">
                <span className="close" onClick={() => setIsOpen(false)}>
                  &times;
                </span>
                {currentCardAction.card.currentStage === undefined || (
                  <div className="card">
                    <CurrentCardComponent
                      stage={
                        currentCardAction.card.stage[
                          currentCardAction.card.currentStage
                        ]
                      }
                      resourceToText={resourceToText}
                      cardOrigin={currentCardAction.card}
                      action={currentCardAction.action}
                      updateCurrentCardActionResource={(index: number) =>
                        setCurrentCardActionResource(index)
                      }
                    />
                  </div>
                )}
                <div className="card">
                  {currentStockedCard.map((card, index1) => {
                    const index = card.stage.findIndex(
                      (stage) => stage.id === card.currentStage
                    );
                    if (index === -1) {
                      return <p>an error occured</p>;
                    }
                    return (
                      <div
                        key={`stocked_card_${card.id}_stage_${
                          card.stage[index % 4].id
                        }`}
                      >
                        <ActiveComponent
                          item={index1}
                          stage={card.stage[index % 4]}
                          resourceToText={resourceToText}
                          cardOrigin={card}
                          addActiveStockedCard={(card, index) =>
                            addActiveStockedCard(card, index)
                          }
                          removeActiveStockedCard={(card) =>
                            removeActiveStockedCard(card)
                          }
                          removeCardFromStock={(card: Card) => {
                            setStockCardToRemove([card]);
                            setIsDiscardNeeded(false);
                            setIsOpen(false);
                          }}
                          isDiscardNeeded={isDiscardNeeded}
                        />
                      </div>
                    );
                  })}
                </div>
                <button
                  onClick={() => {
                    const action = currentCardAction.action;

                    if (action === "unknown") {
                      return;
                    }

                    const resourcesCostOfAction =
                      currentCardAction.card.stage[
                        currentCardAction.card.currentStage
                      ][action];

                    if (resourcesCostOfAction === null) {
                      return;
                    }

                    if (currentCardActionResource === -1) {
                      return;
                    }

                    const resourceNeeded = {
                      ...resourcesCostOfAction[currentCardActionResource],
                    };

                    currentActiveStockedCard.forEach((value) => {
                      const resources =
                        value.card.stage[value.card.currentStage].resources;
                      if (resources === null) {
                        return;
                      }

                      const resourceIndex = value.resourceIndex;
                      if (resourceIndex <= -1) {
                        return;
                      }

                      const resource = resources[resourceIndex];
                      resourceNeeded.cerveau =
                        resourceNeeded.cerveau - resource.cerveau < 0
                          ? 0
                          : resourceNeeded.cerveau - resource.cerveau;
                      resourceNeeded.rouage =
                        resourceNeeded.rouage - resource.rouage < 0
                          ? 0
                          : resourceNeeded.rouage - resource.rouage;
                      resourceNeeded.potion =
                        resourceNeeded.potion - resource.potion < 0
                          ? 0
                          : resourceNeeded.potion - resource.potion;
                      resourceNeeded.danger =
                        resourceNeeded.danger - resource.danger > 0
                          ? 0
                          : resourceNeeded.danger - resource.danger;

                      console.log(
                        resourceNeeded,
                        "vs",
                        resourcesCostOfAction[currentCardActionResource]
                      );
                      if (
                        resourceNeeded.cerveau === 0 &&
                        resourceNeeded.potion === 0 &&
                        resourceNeeded.rouage === 0 &&
                        resourceNeeded.danger === 0
                      ) {
                        action === "stock"
                          ? addStockedCard(currentCardAction.card, true)
                          : updateCardStage(
                              currentCardAction.card,
                              currentCardAction.action,
                              true
                            );
                        removeAllActiveStockedCard();
                        setIsOpen(false);
                        return;
                      }
                    });

                    if (
                      !(
                        resourceNeeded.cerveau === 0 &&
                        resourceNeeded.potion === 0 &&
                        resourceNeeded.rouage === 0 &&
                        resourceNeeded.danger === 0
                      )
                    ) {
                      resourceNeeded.cerveau =
                        resourcesCostOfAction[
                          currentCardActionResource
                        ].cerveau;
                      resourceNeeded.potion =
                        resourcesCostOfAction[currentCardActionResource].potion;
                      resourceNeeded.rouage =
                        resourcesCostOfAction[currentCardActionResource].rouage;
                      resourceNeeded.danger =
                        resourcesCostOfAction[currentCardActionResource].danger;
                    }
                  }}
                >
                  validate
                </button>
              </div>
            </div>
          )}
          {currentStockedCard.length > 0 && (
            <div style={{ marginBottom: "10px" }}>
              <p style={{ fontWeight: "bold", fontSize: "ex" }}>
                Active Resources Cards
              </p>

              <div className={`card template1${currentStockedCard.length}`}>
                {currentStockedCard.map((card, index1) => {
                  const index = card.stage.findIndex(
                    (stage) => stage.id === card.currentStage
                  );
                  if (index === -1) {
                    return <p>an error occured</p>;
                  }
                  return (
                    <ActiveComponent
                      key={`active_card_${card.id}_stage_${
                        card.stage[index % 4].id
                      }`}
                      item={index1}
                      stage={card.stage[index % 4]}
                      resourceToText={resourceToText}
                      cardOrigin={card}
                      addActiveStockedCard={addActiveStockedCard}
                      removeActiveStockedCard={removeActiveStockedCard}
                      removeCardFromStock={(card: Card) => {
                        setStockCardToRemove([card]);
                        setIsDiscardNeeded(false);
                        setIsOpen(false);
                      }}
                      isDiscardNeeded={isDiscardNeeded}
                    />
                  );
                })}
              </div>
            </div>
          )}
          <div className="main">
            {cards
              .filter((value, index) =>
                cards[0].id !== "P"
                  ? index <= 2
                    ? value
                    : null
                  : index <= 1
                  ? value
                  : null
              )
              .map((card, currentIndex) => {
                const index = card.stage.findIndex(
                  (stage) => stage.id === card.currentStage
                );
                if (index === -1) {
                  return <p>an error occured</p>;
                }

                const cardStages = new Array<JSX.Element>(4);

                for (let indexL = 0; indexL < card.stage.length; indexL++) {
                  const orderMatrix: { [x: number]: number[] } = {
                    0: [0, 2, 1, 3],
                    1: [2, 0, 3, 1],
                    2: [1, 3, 0, 2],
                    3: [3, 1, 2, 0],
                  };

                  cardStages[indexL] = (
                    <StageComponent
                      key={card.stage[(index + indexL) % 4].id}
                      stage={card.stage[(index + indexL) % 4]}
                      order={(index + indexL) % 4}
                      resourceToText={resourceToText}
                      resourceToTextWithNumber={resourceToTextWithNumber}
                      cardOrigin={card}
                      disabled={
                        indexL !== 0 ||
                        currentIndex === 2 ||
                        (!card.skip && currentIndex !== 0) ||
                        (!card.canBeUsedIn2 && currentIndex !== 0)
                      }
                      openModal={() => setIsOpen(true)}
                      setCurrentCardAction={(card: Card, action: Action) =>
                        setCurrentCardAction({ card, action })
                      }
                      hidden={
                        currentIndex === 2 ||
                        card.stage[card.currentStage].hidden
                          ? orderMatrix[card.currentStage][
                              (index + indexL) % 4
                            ] === 3 ||
                            orderMatrix[card.currentStage][
                              (index + indexL) % 4
                            ] === 1
                          : false
                      }
                    />
                  );
                }

                const element = (
                  <div
                    className={`card template${card.stage.length}${
                      card.currentStage + 1
                    }`}
                  >
                    {cardStages.map((cardStage) => {
                      return cardStage;
                    })}
                  </div>
                );

                if (currentIndex === 0) {
                  return (
                    <div key={`carte_${card.id}`}>
                      {element}
                      <button
                        onClick={() => {
                          cardIsUsed(card, false, false);
                        }}
                      >
                        discard
                      </button>
                    </div>
                  );
                }

                return (
                  <div
                    key={`carte_${card.id}`}
                    className={`${currentIndex === 2 ? "two-cell" : ""}`}
                  >
                    {element}
                  </div>
                );
              })}
          </div>
        </>
      )}
    </article>
  );
}

function resourceToText(
  resources: null | Resource[],
  emoji?: string
): JSX.Element {
  let resourcesElement = <></>;

  if (resources === null) {
    return resourcesElement;
  }
  resourcesElement = (
    <>
      {resources.map((resource, index) => {
        if (index > 0) {
          return (
            <span key={index}>
              {" "}
              / {Array(resource.cerveau).fill("🧠")}
              {Array(resource.potion).fill("🧪")}
              {Array(resource.rouage).fill("🔩")}
            </span>
          );
        } else {
          return (
            <span key={index}>
              {emoji ? emoji + " : " : ""}{" "}
              {resource.cerveau === resource.potion &&
              resource.potion === resource.rouage &&
              resource.danger === 0 &&
              resource.rouage === 0
                ? "gratuit"
                : ""}
              {Array(resource.cerveau).fill("🧠")}
              {Array(resource.potion).fill("🧪")}
              {Array(resource.rouage).fill("🔩")}
              {resource.danger !== 0 && `${resource.danger}:⏱️`}
            </span>
          );
        }
      })}
    </>
  );

  return resourcesElement;
}

function resourceToTextWithNumber(
  resources: null | Resource[],
  emoji?: string
): JSX.Element {
  let resourcesElement = <></>;

  if (resources === null) {
    return resourcesElement;
  }
  resourcesElement = (
    <>
      {resources.map((resource, index) => {
        if (index > 0) {
          return (
            <span key={index}>
              {" "}
              / {resource.cerveau > 0 && `${resource.cerveau}:🧠`}
              {resource.cerveau > 0 &&
                (resource.potion > 0 || resource.rouage > 0) &&
                " "}
              {resource.potion > 0 && `${resource.potion}:🧪`}
              {resource.potion > 0 && resource.rouage > 0 && " "}
              {resource.rouage > 0 && `${resource.rouage}:🔩`}
              {resource.rouage > 0 && resource.danger !== 0 && " "}
              {resource.danger !== 0 && `${resource.danger}:⏱️`}
            </span>
          );
        } else {
          return (
            <span key={index}>
              {emoji ? emoji + " : " : ""}{" "}
              {resource.cerveau === resource.potion &&
              resource.potion === resource.rouage &&
              resource.rouage === 0 &&
              resource.danger === 0
                ? "gratuit"
                : ""}
              {resource.cerveau > 0 && `${resource.cerveau}:🧠`}
              {resource.cerveau > 0 &&
                (resource.potion > 0 || resource.rouage > 0) &&
                " "}
              {resource.potion > 0 && `${resource.potion}:🧪`}
              {resource.potion > 0 && resource.rouage > 0 && " "}
              {resource.rouage > 0 && `${resource.rouage}:🔩`}
              {resource.rouage > 0 && resource.danger !== 0 && " "}
              {resource.danger !== 0 && `${resource.danger}:⏱️`}
            </span>
          );
        }
      })}
    </>
  );

  return resourcesElement;
}

export default Game;
