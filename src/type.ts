export type Cards = Card[];

export interface Card {
  id: string;
  skip: boolean;
  skipStock: boolean;
  canBeUsedIn2: boolean;
  currentStage: number;
  stage: Stage[];
}

export interface Stage {
  bonus: Bonus;
  hidden: boolean;
  id: number;
  name: string;
  pivot: Resource[] | null;
  resources: Resource[] | null;
  return: Resource[] | null;
  score: number;
  special: Special | null;
  stock: Resource[] | null;
  victory: Resource[] | null;
}

export interface Bonus {
  engin: number;
  potion: number;
}

export interface Resource {
  cerveau: number;
  potion: number;
  rouage: number;
  danger: number;
}

export interface Special {
  multiplier: number;
  resources: string;
}

export type Action = "unknown" | "stock" | "pivot" | "return" | "victory";
