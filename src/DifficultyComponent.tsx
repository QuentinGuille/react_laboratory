interface difficultyProps {
  difficulty: number;
  updateDifficulty: (difficulty: number) => void;
}

const DifficultyComponent = (props: difficultyProps) => {
  return (
    <div className="slideContainer">
      <div className="diffTextContainer">
        <div className="diffText">easy</div>
        <div className="diffText" style={{ left: "33%" }}>
          normal
        </div>
        <div className="diffText" style={{ left: "66%" }}>
          hard
        </div>
        <div className="diffText" style={{ left: "99%" }}>
          harder
        </div>
      </div>
      <input
        type="range"
        min="0"
        max="3"
        value={props.difficulty}
        className="slider"
        id="myRange"
        onChange={(ev) =>
          props.updateDifficulty(Number.parseInt(ev.target.value))
        }
      />
    </div>
  );
};

export default DifficultyComponent;
