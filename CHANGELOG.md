# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] 2024-07-03

#### Style

- navbar added

#### Change

- Component App reworked
- Component Game created
- Component NavBar created
- Danger Card added
- Some change in logic

## [0.0.6] 2024-06-24

#### Fix

- stage 3 to 2 is now correctly taken into account

## [0.0.5] 2024-06-24

#### Feat

- add shuffle
- add assistant card

#### Fix

- correct behavior when stocking card

## [0.0.4] 2024-06-24

#### Fix

- when card is paid to be stocked, it now appear on stocked card

* the bug was due to the fact that the same state was updated on with two different function, once when card was added to stocked card and once when the card used for the action was used

## [0.0.3] 2024-06-21

#### Feat

- added victory condition

#### Fix

- fix copy vs assign of object:

* when user paid for a card if the price was not enough it was deducted on the card

## [0.0.2] 2024-06-21

#### Fix

- danger increase

* previously danger increased even though the condition was met. this was due to the fact that if the card was considered skipped if in cardIsUsed even though it was not always the case. Henceforth I added a variable `paid` to indicated if the card was paid or not (ie: not skipped or skipped)

## [0.0.1] 2024-06-20

#### Feat

- minimum viable product
